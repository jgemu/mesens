/*
 * Mesen-S - Jolly Good API Port
 *
 * Copyright (C) 2019-2020 M. Bibaud (aka Sour)
 * Copyright (C) 2020 Rupert Carmichael
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, specifically version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <string>
#include <sstream>
#include <algorithm>
#include <unordered_map>

#include <jg/jg.h>
#include <jg/jg_snes.h>

#include "gamedb.h"

#include "Core/Console.h"
#include "Core/Spc.h"
#include "Core/BaseCartridge.h"
#include "Core/MemoryManager.h"
#include "Core/VideoDecoder.h"
#include "Core/VideoRenderer.h"
#include "Core/EmuSettings.h"
#include "Core/SaveStateManager.h"
#include "Core/CheatManager.h"

#include "Core/IRenderingDevice.h"
#include "Core/IAudioDevice.h"
#include "Core/SoundMixer.h"

#include "Core/IKeyManager.h"
#include "Core/KeyManager.h"
#include "Core/Console.h"

#include "Utilities/snes_ntsc.h"
#include "Utilities/FolderUtilities.h"

#define SAMPLERATE 48000
#define FRAMERATE 60
#define FRAMERATE_PAL 50
#define CHANNELS 2
#define NUMINPUTS 2

#define ASPECT_NTSC 1.306122
#define ASPECT_PAL 1.4257812

#define TIMING_NTSC 60.098812
#define TIMING_PAL 50.006979

static jg_cb_audio_t jg_cb_audio;
static jg_cb_frametime_t jg_cb_frametime;
static jg_cb_log_t jg_cb_log;
static jg_cb_rumble_t jg_cb_rumble;
static jg_cb_settings_read_t jg_cb_settings_read;

static jg_coreinfo_t coreinfo = {
    "mesens", "Mesen-S", "git", "snes", NUMINPUTS,
    JG_HINT_VIDEO_INTERNAL | JG_HINT_AUDIO_INTERNAL
};

static jg_videoinfo_t vidinfo = {
    JG_PIXFMT_XRGB8888, // pixfmt
    512,                // wmax
    478,                // hmax
    256,                // w
    224,                // h
    0,                  // x
    7,                  // y
    256,                // p
    8.0/7.0,            // aspect
    NULL
};

static jg_audioinfo_t audinfo = {
    JG_SAMPFMT_INT16,
    SAMPLERATE,
    CHANNELS,
    (SAMPLERATE / FRAMERATE) * CHANNELS,
    NULL
};

static jg_pathinfo_t pathinfo;
static jg_fileinfo_t gameinfo;
static jg_inputinfo_t inputinfo[NUMINPUTS];
static jg_inputstate_t *input_device[NUMINPUTS];

// Emulator settings
static jg_setting_t settings_mesens[] = { // name, default, min, max
    { "aspect_ratio", "", 0, 0, 1 }, // 0 = 8:7, 1 = Auto Region
    { "ntsc_filter", "", 0, 0, 1 }, // 0 = Disable, 1 = Enable
};

enum {
    ASPECT,
    NTSC,
};

// Mesen-S class definitions
class JollyGoodRenderer : public IRenderingDevice {
private:
    shared_ptr<Console> _console;

public:
    JollyGoodRenderer(shared_ptr<Console> console) {
        _console = console;
        _console->GetVideoRenderer()->RegisterRenderingDevice(this);
    }

    ~JollyGoodRenderer() {
        _console->GetVideoRenderer()->UnregisterRenderingDevice(this);
    }

    // Inherited via IRenderingDevice
    virtual void UpdateFrame(void *frameBuffer, uint32_t width, uint32_t height)
        override {
        
        int multiplier = (height / 239);
        vidinfo.y = (8 * multiplier);
        vidinfo.w = width;
        vidinfo.h = (height - (vidinfo.y * 2)) + multiplier;
        vidinfo.p = width; // Set the pitch
        vidinfo.y -= multiplier; // Compensate for the missing line
        vidinfo.buf = frameBuffer;
    }
    
    virtual void Render() override {}
    virtual void Reset() override {}
    virtual void SetFullscreenMode(bool fullscreen, void *windowHandle,
        uint32_t monitorWidth, uint32_t monitorHeight) override {}
};

class JollyGoodSoundManager : public IAudioDevice {
private:
    shared_ptr<Console> _console;

public:
    JollyGoodSoundManager(shared_ptr<Console> console) {
        _console = console;
        _console->GetSoundMixer()->RegisterAudioDevice(this);
    }

    ~JollyGoodSoundManager() {
        _console->GetSoundMixer()->RegisterAudioDevice(nullptr);
    }
    
    // Inherited via IAudioDevice
    virtual void PlayBuffer(int16_t *soundBuffer, uint32_t sampleCount,
        uint32_t sampleRate, bool isStereo) override {
        
        int channels = isStereo ? 2 : 1;
        audinfo.buf = static_cast<void*>(soundBuffer);
        jg_cb_audio(sampleCount * channels);
    }

    virtual void Stop() override {}

    virtual void Pause() override {}

    virtual string GetAvailableDevices() override {
        return string();
    }

    virtual void SetAudioDevice(string deviceName) override {}
    virtual void ProcessEndOfFrame() override {}

    virtual AudioStatistics GetStatistics() override {
        return AudioStatistics();
    }
};

class JollyGoodKeyManager : public IKeyManager
{
private:
    shared_ptr<Console> _console;
    bool _mouseButtons[3] = { false, false, false };
    jg_inputstate_t *pdev = nullptr;

public:
    int pos_offset[2] = { 0, 0 };
    
    JollyGoodKeyManager(shared_ptr<Console> console) {
        _console = console;
        KeyManager::RegisterKeyManager(this);
    }

    ~JollyGoodKeyManager() {
        KeyManager::RegisterKeyManager(nullptr);
    }
    
    void setPointerDevice(jg_inputstate_t *pdev) {
        this->pdev = pdev;
    }
    
    // Inherited via IKeyManager
    virtual void RefreshState() override {
        if (pdev == nullptr) return;
        int32_t x = pdev->coord[0];
        int32_t y = pdev->coord[1];
        
        MousePosition mp = KeyManager::GetMousePosition();
        int16_t dx = x - mp.X;
        int16_t dy = y - mp.Y;
        
        x += pos_offset[0];
        y += pos_offset[1];
        
        KeyManager::SetMousePosition(_console,
            (double)x / vidinfo.w,
            (double)y / (vidinfo.w > 256 ? 478 : 239));
        
        KeyManager::SetMouseMovement(dx, dy);
        
        _mouseButtons[(int)MouseButton::LeftButton] = pdev->button[0];
        _mouseButtons[(int)MouseButton::RightButton] = pdev->button[1];
        _mouseButtons[(int)MouseButton::MiddleButton] = pdev->button[2];
    }
    
    virtual bool IsKeyPressed(uint32_t keyCode) override {
        return keyCode ?
            input_device[keyCode >> 8]->button[(keyCode - 1) & 0xff] : false;
    }

    virtual void UpdateDevices() override {}

    virtual bool IsMouseButtonPressed(MouseButton button) override {
        return _mouseButtons[(int)button];
    }

    virtual vector<uint32_t> GetPressedKeys() override {
        return vector<uint32_t>();
    }
    
    virtual string GetKeyName(uint32_t keyCode) override {
        return string();
    }

    virtual uint32_t GetKeyCode(string keyName) override {
        return 0;
    }

    virtual void SetKeyState(uint16_t scanCode, bool state) override {
    }

    virtual void ResetKeyState() override {
    }

    virtual void SetDisabled(bool disabled) override {
    }
};

static std::shared_ptr<Console> _console;
static std::shared_ptr<BatteryManager> _batteryManager;
static std::unique_ptr<JollyGoodRenderer> _renderer;
static std::unique_ptr<JollyGoodSoundManager> _soundManager;
static std::unique_ptr<JollyGoodKeyManager> _keyManager;

static InputConfig input;
static VideoConfig video;

auto getKeyCode (int port, int jollyKey) {
    return (port << 8) | (jollyKey + 1);
};

auto getKeyBindings(int port) {
    KeyMappingSet keyMappings;
    keyMappings.TurboSpeed = 0;
    keyMappings.Mapping1.Up = getKeyCode(port, 0);
    keyMappings.Mapping1.Down = getKeyCode(port, 1);
    keyMappings.Mapping1.Left = getKeyCode(port, 2);
    keyMappings.Mapping1.Right = getKeyCode(port, 3);
    keyMappings.Mapping1.Select = getKeyCode(port, 4);
    keyMappings.Mapping1.Start = getKeyCode(port, 5);
    keyMappings.Mapping1.A = getKeyCode(port, 6);
    keyMappings.Mapping1.B = getKeyCode(port, 7);
    keyMappings.Mapping1.X = getKeyCode(port, 8);
    keyMappings.Mapping1.Y = getKeyCode(port, 9);
    keyMappings.Mapping1.L = getKeyCode(port, 10);
    keyMappings.Mapping1.R = getKeyCode(port, 11);
    return keyMappings;
};

// Jolly Good API Calls
void jg_set_cb_audio(jg_cb_audio_t func) {
    jg_cb_audio = func;
}

void jg_set_cb_frametime(jg_cb_frametime_t func) {
    jg_cb_frametime = func;
}

void jg_set_cb_log(jg_cb_log_t func) {
    jg_cb_log = func;
}

void jg_set_cb_rumble(jg_cb_rumble_t func) {
    jg_cb_rumble = func;
}

void jg_set_cb_settings_read(jg_cb_settings_read_t func) {
    jg_cb_settings_read = func;
}

int jg_init() {
    jg_cb_settings_read(settings_mesens,
        sizeof(settings_mesens) / sizeof(jg_setting_t));
    
    _console.reset(new Console());
    _console->Initialize();
    KeyManager::SetSettings(_console->GetSettings().get());
    
    _renderer.reset(new JollyGoodRenderer(_console));
    _soundManager.reset(new JollyGoodSoundManager(_console));
    _keyManager.reset(new JollyGoodKeyManager(_console));
    
    AudioConfig audioConfig = _console->GetSettings()->GetAudioConfig();
    audioConfig.DisableDynamicSampleRate = false;
    audioConfig.SampleRate = SAMPLERATE;
    _console->GetSettings()->SetAudioConfig(audioConfig);
    
    PreferencesConfig preferences = _console->GetSettings()->GetPreferences();
    preferences.DisableOsd = true;
    preferences.RewindBufferSize = 0;
    _console->GetSettings()->SetPreferences(preferences);
    
    FolderUtilities::SetFolderOverrides(std::string(pathinfo.save),
        "", std::string(pathinfo.bios));
    
    return 1;
}

void jg_deinit() {
    _renderer.reset();
    _soundManager.reset();
    _keyManager.reset();
    _console->Release();
    _console.reset();
}

void jg_reset(int hard) {
    _console->Reset();
}

void jg_exec_frame() {
    _console->RunSingleFrame();
}

int jg_game_load() {
    std::string fn(gameinfo.fname);
    if(fn.substr(fn.find_last_of(".") + 1) == "bs") {
        std::string bsx_bios = std::string(pathinfo.bios) + "/BsxBios.sfc";
        std::ifstream f(bsx_bios.c_str());
        if (!f.good()) {
            jg_cb_log(JG_LOG_ERR,
                "Missing BIOS file \'BsxBios.sfc\', exiting...\n");
            return 0;
        }
    }
    
    VirtualFile romData(gameinfo.data, gameinfo.size, gameinfo.path);
    VirtualFile patch;
    bool result = _console->LoadRom(romData, patch);
    
    input = _console->GetSettings()->GetInputConfig();
    video = _console->GetSettings()->GetVideoConfig();
    
    // Set up Input devices
    inputinfo[0] = jg_snes_inputinfo(0, JG_SNES_PAD);
    input.Controllers[0].Type = ControllerType::SnesController;
    input.Controllers[0].Keys = getKeyBindings(0);
    
    inputinfo[1] = jg_snes_inputinfo(1, JG_SNES_PAD);
    input.Controllers[1].Type = ControllerType::SnesController;
    input.Controllers[1].Keys = getKeyBindings(1);
    
    // Detect Super Scope games
    std::unordered_map<std::string, std::pair<int, int>>::iterator iter_ss =
        ss_games.find(std::string(gameinfo.md5));
    
    if (iter_ss != ss_games.end()) {
        inputinfo[1] = jg_snes_inputinfo(1, JG_SNES_SUPERSCOPE);
        input.Controllers[1].Type = ControllerType::SuperScope;
        
        _keyManager->setPointerDevice(input_device[1]);
        _keyManager->pos_offset[0] = iter_ss->second.first;
        _keyManager->pos_offset[1] = iter_ss->second.second;
    }
    
    // Detect SNES Mouse games
    std::unordered_map<std::string, int>::iterator iter_mouse =
        mouse_games.find(std::string(gameinfo.md5));
    
    if (iter_mouse != mouse_games.end()) {
        int port = iter_mouse->second;
        
        inputinfo[port] = jg_snes_inputinfo(port, JG_SNES_MOUSE);
        input.Controllers[port].Type = ControllerType::SnesMouse;
        
        _keyManager->setPointerDevice(input_device[port]);
    }
    
    // Set region-specific values
    ConsoleRegion region = _console->GetCartridge()->GetRegion();
    
    // Aspect ratio
    if (settings_mesens[ASPECT].value)
        vidinfo.aspect =
            region == ConsoleRegion::Pal ? ASPECT_PAL : ASPECT_NTSC;
    
    // Frame timing
    if (region == ConsoleRegion::Pal) {
        audinfo.spf = (SAMPLERATE / FRAMERATE_PAL) * CHANNELS;
        jg_cb_frametime(TIMING_PAL);
    }
    else {
        jg_cb_frametime(TIMING_NTSC);
    }
    
    // Set NTSC Filter if enabled
    if (settings_mesens[NTSC].value) {
        video.VideoFilter = VideoFilterType::NTSC;
        vidinfo.wmax = 602;
    }
    
    // Feed settings back into the emulator core
    _console->GetSettings()->SetInputConfig(input);
    _console->GetSettings()->SetVideoConfig(video);
    
    return result ? 1 : 0; // boolean to int
}

int jg_game_unload() {
    _console->Stop(false);
    return 1;
}

int jg_state_load(const char *filename) {
    return _console->GetSaveStateManager()->LoadState(std::string(filename));
}

int jg_state_save(const char *filename) {
    return _console->GetSaveStateManager()->SaveState(std::string(filename));
}

void jg_media_select() {
}

void jg_media_insert() {
}

void jg_cheat_clear() {
}

void jg_cheat_set(const char *code) {
    if (code) { }
}

// JG Functions that return values to the frontend
jg_coreinfo_t* jg_get_coreinfo(const char *sys) {
    return &coreinfo;
}

jg_videoinfo_t* jg_get_videoinfo() {
    return &vidinfo;
}

jg_audioinfo_t* jg_get_audioinfo() {
    return &audinfo;
}

jg_inputinfo_t* jg_get_inputinfo(int port) {
    return &inputinfo[port];
}

// JG Functions that set values in the emulator core
void jg_setup_video() {
}

void jg_setup_audio() {
}

void jg_set_inputstate(jg_inputstate_t *ptr, int port) {
    input_device[port] = ptr;
}

void jg_set_gameinfo(jg_fileinfo_t info) {
    gameinfo = info;
}

void jg_set_auxinfo(jg_fileinfo_t info, int index) {
}

void jg_set_paths(jg_pathinfo_t paths) {
    pathinfo = paths;
}
