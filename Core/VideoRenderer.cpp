#include "stdafx.h"
#include "IRenderingDevice.h"
#include "VideoRenderer.h"
#include "VideoDecoder.h"
#include "Console.h"
#include "EmuSettings.h"
#include "MessageManager.h"
#include "../Utilities/IVideoRecorder.h"
#include "../Utilities/GifRecorder.h"

VideoRenderer::VideoRenderer(shared_ptr<Console> console)
{
	_console = console;
	_stopFlag = false;	
	StartThread();
}

VideoRenderer::~VideoRenderer()
{
	_stopFlag = true;
	StopThread();
}

void VideoRenderer::StartThread()
{
}

void VideoRenderer::StopThread()
{
}

void VideoRenderer::RenderThread()
{
	if(_renderer) {
		_renderer->Reset();
	}

	while(!_stopFlag.load()) {
		//Wait until a frame is ready, or until 16ms have passed (to allow UI to run at a minimum of 60fps)
		_waitForRender.Wait(16);
		if(_renderer) {
			_renderer->Render();
		}
	}
}

void VideoRenderer::UpdateFrame(void* frameBuffer, uint32_t width, uint32_t height)
{
	shared_ptr<IVideoRecorder> recorder = _recorder;
	if(recorder) {
		recorder->AddFrame(frameBuffer, width, height, _console->GetFps());
	}

	if(_renderer) {
		_renderer->UpdateFrame(frameBuffer, width, height);
		_waitForRender.Signal();
	}
}

void VideoRenderer::RegisterRenderingDevice(IRenderingDevice *renderer)
{
	_renderer = renderer;
	StartThread();
}

void VideoRenderer::UnregisterRenderingDevice(IRenderingDevice *renderer)
{
	if(_renderer == renderer) {
		StopThread();
		_renderer = nullptr;
	}
}
